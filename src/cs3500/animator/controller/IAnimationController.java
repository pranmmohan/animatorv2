package cs3500.animator.controller;

import java.util.List;

/**
 * Interface to contain methods for interacting with views using controller.
 */
public interface IAnimationController {

  /**
   * Render the view.
   */
  public void render();

  /**
   * Render/start the controller.
   */
  public void run();

  /**
   * Pause/stop entire animation.
   */
  public void pause();

  /**
   * Start the animation.
   */
  public void start();

  /**
   * Restart the entire animation.
   */
  public void restart();

  /**
   * Loop the animation.
   */
  public void loop();

  /**
   * Create lists of shapes that should appear and shapes that shouldn't(check boxed).
   * @param checked     list of shapes to disappear.
   * @param notChecked  list of shapes to appear.
   */
  public void checked(List<String> checked, List<String> notChecked);

  /**
   * Set the speed of the animation.
   * @param speed   speed.
   */
  public void setSpeed(int speed);

  /**
   * Export SVG to a file.
   */
  public void exportSVG();


}
