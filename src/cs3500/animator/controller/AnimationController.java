package cs3500.animator.controller;

import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Animator;
import cs3500.animator.view.Interactive;
import cs3500.animator.view.View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 * Controller GUI to interact with hybrid view.
 */
public class AnimationController implements ActionListener, IAnimationController {

  private Animator model;
  private View view;
  private int width;
  private int height;
  private int speed;
  private Appendable ap;
  private boolean loop;
  private JFrame interactionFrame = new JFrame();
  private JFrame selectShapesFrame = new JFrame();
  private JButton shapeShow = new JButton("Show/Hide Shapes");
  private JButton startStopButton;
  private boolean timerRunning = true;
  private OutputStream outWrite;

  /**
   * Controller to provide GUI for user to interact with view of animations.
   * @param model     model.
   * @param view      view.
   * @param width     width.
   * @param height    height.
   * @param speed     speed.
   * @param ap        appendable.
   * @param outWrite  output file.
   */
  public AnimationController(Animator model, View view, int width, int height, int speed,
                             Appendable ap, OutputStream outWrite) {
    this.model = model;
    this.view = view;
    this.width = width;
    this.height = height;
    this.speed = speed;
    this.loop = false;
    this.ap = ap;
    this.outWrite = outWrite;
  }

  /**
   * Render the view.
   */
  public void render() {
    view.render(width, height, speed, ap);
  }

  /**
   * Render/start the controller.
   */
  public void run() {

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    view.render(width, height, speed, ap);

    interactionFrame.setTitle("Controller");
    interactionFrame.setLocation((int) ((int)screenSize.getWidth() - (0.1
            * (int)screenSize.getWidth())), (int) ((int)screenSize.getHeight()
            - (0.1 * (int)screenSize.getHeight())));
    interactionFrame.setSize(700, 700);
    interactionFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    interactionFrame.add(makeSpeedPanel(), BorderLayout.LINE_END);
    interactionFrame.add(makeButtonPanel(), BorderLayout.LINE_START);
    interactionFrame.add(shapeShow);
    interactionFrame.setVisible(true);

    selectShapesFrame.setTitle("Select Shapes");
    selectShapesFrame.setSize(200, 700);
    selectShapesFrame.setVisible(false);

    shapeShow.addActionListener(e -> {
      JPanel checkBoxPanel = makeCheckboxPanel();
      JScrollPane scrollPane = new JScrollPane(checkBoxPanel);
      scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      scrollPane.getVerticalScrollBar().setUnitIncrement(16);
      selectShapesFrame.add(scrollPane);
      selectShapesFrame.setVisible(true);
    });

  }

  /**
   * Pause/stop entire animation.
   */
  public void pause() {
    Interactive views = (Interactive) view;
    views.stop();
  }

  /**
   * Start the animation.
   */
  public void start() {
    Interactive views = (Interactive) view;
    views.start();
  }

  /**
   * Restart the entire animation.
   */
  public void restart() {
    Interactive views = (Interactive) view;
    views.restart();
  }

  /**
   * Loop the animation.
   */
  public void loop() {
    Interactive views = (Interactive) view;
    views.loop();
  }

  public void exportSVG() {
    Interactive views = (Interactive) view;
    views.exportSVG();
  }

  /**
   * Create lists of shapes that should appear and shapes that shouldn't(check boxed).
   * @param checked     list of shapes to disappear.
   * @param notChecked  list of shapes to appear.
   */
  public void checked(List<String> checked, List<String> notChecked) {
    Interactive views = (Interactive) view;
    views.setAppear(checked, notChecked);
  }

  /**
   * Set the speed of the animation.
   * @param speed   speed.
   */
  public void setSpeed(int speed) {
    Interactive views = (Interactive) view;
    views.setSpeed(speed);
  }

  /**
   * Create panel of checkboxes used to select shapes that should be hidden.
   * @return panel of checkboxes.
   */
  private JPanel makeCheckboxPanel() {
    JPanel checkBoxPanel = new JPanel(new GridLayout(0, 2,2,2));
    JButton submit = new JButton("Submit");
    checkBoxPanel.setBorder(BorderFactory.createTitledBorder("Shape(s) to hide"));
    checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.PAGE_AXIS));

    List<JCheckBox> checkBoxes = new ArrayList<>();

    for (int i = 0; i < model.getShapes().size(); i++) {
      checkBoxes.add(new JCheckBox(model.getShapes().get(i).getName()));
      checkBoxPanel.add(checkBoxes.get(i));
    }

    submit.addActionListener(e -> {
      List<String> checked = new ArrayList<>();
      List<String> notchecked = new ArrayList<>();
      if (!timerRunning) {
        List<AbstractShape> shapes = model.getShapes();
        for (int i = 0 ; i < model.getShapes().size(); i++) {
          if (checkBoxes.get(i).isSelected()) {
            checked.add(shapes.get(i).getName());
          }
          else {
            notchecked.add(shapes.get(i).getName());
          }
        }
      }
      checked(checked, notchecked);
    });
    checkBoxPanel.add(submit);

    return checkBoxPanel;
  }

  /**
   * Make panel for changing speed of animation.
   * @return speed panel.
   */
  private JPanel makeSpeedPanel() {
    JPanel speedPanel = new JPanel();
    speedPanel.setLayout(new BoxLayout(speedPanel, BoxLayout.PAGE_AXIS));
    JLabel currentSpeed = new JLabel("Speed is: ");
    speedPanel.add(currentSpeed);
    JButton increaseSpeed = new JButton("+ Speed");
    JButton decreaseSpeed = new JButton("- Speed");

    increaseSpeed.addActionListener(e -> {
      speed += 5;
      setSpeed(speed);
      currentSpeed.setText("Speed is: " + speed);
    });
    speedPanel.add(increaseSpeed);

    decreaseSpeed.addActionListener(e -> {
      speed -= 5;
      setSpeed(speed);
      currentSpeed.setText("Speed is: " + speed);
    });
    speedPanel.add(decreaseSpeed);

    return speedPanel;
  }

  /**
   * Button panel for JFrame that includes user interactivity.
   * @return button panel.
   */
  private JPanel makeButtonPanel() {
    Timer t = new Timer(500 / speed, this);
    JPanel buttonPanel;
    JButton exitButton;
    JButton restartButton;
    JButton exportSVGButton;
    JCheckBox loopCheckBox;
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    buttonPanel = new JPanel(new GridLayout(0, 1));
    buttonPanel.setBorder(BorderFactory.createLineBorder(Color.darkGray, 5));
    buttonPanel.repaint();
    startStopButton = new JButton("Stop");
    startStopButton.setOpaque(true);
    startStopButton.setPreferredSize(new Dimension((int) (0.1 * screenSize.getWidth()),
            (int) (0.1 * screenSize.getHeight())));
    startStopButton.setBackground(Color.green);
    startStopButton.addActionListener(ae -> {
      if (!timerRunning) {
        startStopButton.setText("Stop");
        this.start();
        timerRunning = true;
      } else if (timerRunning) {
        startStopButton.setText("Start");
        this.pause();
        timerRunning = false;
      }
    });
    startStopButton.setBorder(BorderFactory.createLineBorder(Color.gray.darker(), 4));
    buttonPanel.add(startStopButton);

    restartButton = new JButton("Restart");
    restartButton.setOpaque(true);
    restartButton.setPreferredSize(new Dimension((int) (0.1 * screenSize.getWidth()),
            (int) (0.1 * screenSize.getHeight())));
    restartButton.setBackground(Color.yellow);
    restartButton.addActionListener(ae -> {
      this.restart();
      if (!timerRunning) {
        startStopButton.setText("Stop");
        timerRunning = true;
      }
    });
    restartButton.setBorder(BorderFactory.createLineBorder(Color.gray.darker(), 4));
    buttonPanel.add(restartButton);

    loopCheckBox = new JCheckBox("Loop");
    loopCheckBox.setOpaque(true);
    loopCheckBox.setPreferredSize(new Dimension((int) (0.1 * screenSize.getWidth()),
            (int) (0.1 * screenSize.getHeight())));
    loopCheckBox.setBackground(Color.orange);
    loopCheckBox.setBorder(BorderFactory.createLineBorder(Color.gray.darker(), 4));
    loopCheckBox.addActionListener(ae -> {
      this.loop = !loop;
      loop();
    });
    buttonPanel.add(loopCheckBox);

    exportSVGButton = new JButton("Export SVG");
    exportSVGButton.setOpaque(true);
    exportSVGButton.setBackground(Color.lightGray);
    exportSVGButton.setPreferredSize(new Dimension((int) (0.1 * screenSize.getWidth()),
            (int) (0.1 * screenSize.getHeight())));
    exportSVGButton.addActionListener(e -> {
      exportSVG();
      String outputFile;
      outputFile = JOptionPane.showInputDialog(null,
              "Enter output file name.");

      try {
        FileWriter fw = new FileWriter(outputFile);
        BufferedWriter toFile = new BufferedWriter(fw);
        toFile.write(outWrite.toString());
        toFile.close();
        fw.close();

      }
      catch (IOException ex) {
        throw new IllegalStateException("Cannot create file to write output to");
      }
    });
    exportSVGButton.setBorder(BorderFactory.createLineBorder(Color.gray.darker(), 4));
    buttonPanel.add(exportSVGButton);

    exitButton = new JButton("Exit");
    exitButton.setOpaque(true);
    exitButton.setBackground(Color.red);
    exitButton.setPreferredSize(new Dimension((int) (0.1 * screenSize.getWidth()),
            (int) (0.1 * screenSize.getHeight())));
    exitButton.addActionListener(ae -> {
      t.stop();
      System.exit(0);
    });
    exitButton.setBorder(BorderFactory.createLineBorder(Color.gray.darker(), 4));
    buttonPanel.add(exitButton);

    return buttonPanel;
  }

  /**
   * Action function required to implement Actionlistener.
   * @param e actionevent.
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    // empty because the controller itself does not need to change per click of the timer, the
    // timer is used to synchronize with the view.
  }
}
