package cs3500.animator.view;

import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;

import java.util.List;

/**
 * Interface for each View, since all views will need a list of animations and shape data.
 */
public interface View {

  /**
   * Create the list of shapes and animations.
   *
   * @param shapes     list of shapes.
   * @param animations list of animations.
   */
  void initialize(List<AbstractShape> shapes, List<AbstractAnimation> animations);

  /**
   * Method to initialize dimensions of shape and appendable to be written to.
   *
   * @param width    width of shape.
   * @param height   height of shape.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  void render(int width, int height, int tickrate, Appendable ap);
}
