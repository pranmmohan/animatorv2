package cs3500.animator.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Coordinate;

/**
 * Class for Hybrid view which contains interactive functionality.
 */
public class Hybrid extends Visual implements Interactive {


  private JFrame mainFrame = new JFrame();
  private DrawHybridAnimation da;
  private boolean loop;


  /**
   * Constructor that calls parent class constructor.
   */
  public Hybrid() {
    super();
    loop = false;
  }

  @Override
  public void loop() {
    this.loop = !this.loop;
  }

  @Override
  public void start() {
    this.da.t2.start();
  }

  @Override
  public void stop() {
    this.da.t2.stop();
  }

  @Override
  public void restart() {
    this.da.currenttick = 0;
    for (int i = 0; i < shapes.size(); i++) {
      shapes.get(i).copy(originalshapes.get(i));

    }
    this.da.t2.restart();
  }

  @Override
  public void setAppear(List<String> disappear, List<String> appear) {
    for (String name : disappear) {
      this.appears.put(name, false);
    }
    for (String name : appear) {
      this.appears.put(name, true);
    }
  }

  @Override
  public void setSpeed(int speed) {
    this.da.t2.setDelay(500 / speed);
  }

  @Override
  public void exportSVG() {
    if (!loop) {
      SVGView svg = new SVGView();
      svg.initialize(originalshapes, animations);
      svg.render(width, height, tickrate, ap);
    }
    else {
      SVGloop svgloop = new SVGloop();
      svgloop.initialize(originalshapes, animations);
      svgloop.render(width, height, tickrate, ap);
    }
  }

  /**
   * Method to initialize dimensions of appendable to be written to.
   *
   * @param width    width.
   * @param height   height.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;

    da = new DrawHybridAnimation(shapes);

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    mainFrame.setTitle("Animator Visual View");
    mainFrame.setLocation(0, 0);
    mainFrame.setSize((int) screenSize.getWidth(), (int) screenSize.getHeight());
    mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    mainFrame.add(da);
    mainFrame.setVisible(true);
    da.t2.start();
  }

  /**
   * Class to draw the visual animations using action listeners.
   */
  private class DrawHybridAnimation extends DrawAnimation {

    private int currenttick = 0;

    /**
     * Constructor initializing list of shapes.
     *
     * @param shapes list of shapes.
     */
    public DrawHybridAnimation(List<AbstractShape> shapes) {
      super(shapes);
    }


    protected Timer t2 = new Timer(500 / tickrate, new ActionListener() {
      /**
       * Method to change values of shape based on animation type and redraw using paintComponent.
       * @param e ActionEvent.
       */
      @Override
      public void actionPerformed(ActionEvent e) {

        if (loop & currenttick > totalTime) {
          restart();
          currenttick = 0;
        }

        currenttick++;

        for (AbstractShape s : shapes) {
          alterShape(s, currenttick);
        }
        repaint();
      }
    });

    public void paintComponent(Graphics gr) {
      Graphics2D g = (Graphics2D) gr;
      super.paintComponent(g);

      for (AbstractShape s : shapes) {
        String type = s.getClassName();
        Coordinate c = s.getPosition();
        Color color = s.getColor();
        int width = (int) s.getWidth();
        int height = (int) s.getHeight();
        switch (type) {
          case "Rectangle":
            g.setColor(color);
            if (currenttick >= s.getTStart() && currenttick <= s.getTDissapear() &&
                    appears.get(s.getName())) {
              g.fillRect((int) c.getXCoord(), (int) c.getYCoord(), width, height);
            }
            break;
          case "Oval":
            g.setColor(color);
            if (currenttick >= s.getTStart() && currenttick <= s.getTDissapear() &&
                    appears.get(s.getName())) {
              g.fillOval((int) c.getXCoord(), (int) c.getYCoord(), width, height);
            }
            break;
          default:
            break;
        }
      }
    }
  }
} // end of hybrid class
