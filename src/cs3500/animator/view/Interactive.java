package cs3500.animator.view;

import java.util.List;

/**
 * Interface for interactive actions in the hybrid view.
 */
public interface Interactive {

  /**
   * Restart the entire animation.
   */
  public void restart();

  /**
   * Start the animation.
   */
  public void start();

  /**
   * Pause/stop entire animation.
   */
  public void stop();

  /**
   * Loop the animation.
   */
  public void loop();

  /**
   * Create lists of shapes that should appear and shapes that shouldn't(check boxed).
   * @param checked     list of shapes to disappear.
   * @param notChecked  list of shapes to appear.
   */
  public void setAppear(List<String> checked, List<String> notChecked);

  /**
   * Set the speed of the animation.
   * @param speed   speed.
   */
  public void setSpeed(int speed);

  /**
   * Expor the svg to a file.
   */
  public void exportSVG();

}
