package cs3500.animator.view;


import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Coordinate;

import java.awt.Color;
import java.io.IOException;


/**
 * Requirements must add a svg shape string for each shape.
 */
public class SVGView extends AbstractView {


  protected boolean loop = false;

  /**
   * Constructor for SVG view.
   */
  public SVGView() {

    super();
  }


  /**
   * Initialize shape dimensions, tickrate, and appendable.
   *
   * @param width    Width of shape.
   * @param height   height of shape.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  @Override
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;
    makenonloop();
  }

  protected void makenonloop() {
    makeHeader();
    makeSVG();
    endSVG();
  }

  protected void makeHeader() {
    try {
      String out =
              "<svg width = \"" + width / 100 + "\"" + " height = \"" + height / 100 + "\""
                      + " version = \"1.1\"" + " xmlns = \"http://www.w3.org/2000/svg\"" + ">" + "\n\n";
      ap.append(out);
    } catch (IOException e) {
      throw new IllegalStateException("Throw error");

    }

  }

  /**
   * Print string of color components.
   *
   * @param c Color.
   * @return color as RGB values.
   */
  protected String makeColor(Color c) {
    return "rgb(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + ")";
  }

  /**
   * Print animation as a string.
   *
   * @param variable variable of shape being changed.
   * @param from     start value.
   * @param to       end value.
   * @param start    start of animation.
   * @param end      end of animation.
   * @return animation.
   */
  protected String makeAnimationString(String variable, String from, String to, float start,
                                       float end) {
    String out = "";

    if (!loop) {
      out += "<animate begin =" + "\"" + start / tickrate + "\"" + " attributeName ="
              + "\"" + variable + "\"" + " from = \"" + from + "\" " + "to = \""
              + to + "\"" + " dur = \"" + (end / tickrate - start / tickrate) + "\""
              + " fill=" + "\"freeze\"" + " />\n";
    } else {
      out += "<animate begin =" + "\"base.begin + " + start / tickrate + "\"" + " attributeName ="
              + "\"" + variable + "\"" + " from = \"" + from + "\" " + "to = \""
              + to + "\"" + " dur = \"" + (end / tickrate - start / tickrate) + "\""
              + " fill=" + "\"freeze\"" + " />\n";


    }
    return out;
  }

  /**
   * Set formatter.
   *
   * @param variable variable of shape being changed.
   * @param from     start value.
   * @param to       end value.
   * @param start    start of animation.
   * @return attribute setter.
   */
  protected String makeSetterString(String variable, String from, String to, float start) {
    String out = "";
    out += "<set attributeName = \"" + variable + "\" from = \"" + from + "\" to = \"" + to + "\" "
            + "begin = \"" + start / tickrate + "\"/>\n";
    return out;
  }

  private String shapeEnd(String attributeName, String to) {
    return "<animate  begin=\"base.end\" dur=\"1ms\" attributeName=\"" + attributeName + "\" "
            + "to=\"" + to + "\" fill=\"freeze\" />\n";
  }

  private String svgloop(AbstractShape shape) {
    String out = "";
    Coordinate coord = shape.getPosition();
    if (shape.getClassName().equals("Rectangle") && appears.get(shape.getName())) {
      out += shapeEnd("x", Float.toString(coord.getXCoord()));
      out += shapeEnd("y", Float.toString(coord.getYCoord()));
      out += shapeEnd("width", Float.toString(shape.getWidth()));
      out += shapeEnd("height", Float.toString(shape.getHeight()));
      out += shapeEnd("fill", makeColor(shape.getColor()));
    } else if (shape.getClassName().equals("Oval") && appears.get(shape.getName())) {
      float x = coord.getXCoord() + shape.getWidth() / 2;
      float y = coord.getYCoord() + shape.getHeight() / 2;
      out += shapeEnd("cx", Float.toString(x));
      out += shapeEnd("cy", Float.toString(y));
      out += shapeEnd("rx", Float.toString(shape.getWidth() / 2));
      out += shapeEnd("ry", Float.toString(shape.getHeight() / 2));
      out += shapeEnd("fill", makeColor(shape.getColor()));
    }

    return out;

  }


  /**
   * Create animation strings.
   *
   * @param shape shape.
   * @return all the animations.
   */
  protected String makeAnimations(AbstractShape shape) {
    String out = "";

    if (!loop) {
      out += "<set attributeName=\"visibility\" from=\"hidden\" to=\"visible\" begin=\""
              + shape.getTStart() / tickrate + "\"/>" + "\n";
      out += "<set attributeName=\"visibility\" from=\"visible\" to=\"hidden\" begin=\""
              + shape.getTDissapear() / tickrate + "\"/>" + "\n";
    } else {
      out += "<set attributeName=\"visibility\" from=\"hidden\" to=\"visible\" "
              + "begin=\"base.begin + "
              + shape.getTStart() / tickrate + "\"/>" + "\n";
      out += "<set attributeName=\"visibility\" from=\"visible\" to=\"hidden\" "
              + "begin=\"base.begin + "
              + shape.getTDissapear() / tickrate + "\"/>" + "\n";

    }

    if (shape.getClassName().equals("Rectangle")) {
      for (AbstractAnimation animation : shape.getAnimations()) {
        switch (animation.getClassName()) {

          case "Move":
            Coordinate start = (Coordinate) animation.getStart();
            Coordinate end = (Coordinate) animation.getEnd();
            out += makeAnimationString("x", Float.toString(start.getXCoord()),
                    Float.toString(end.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("y", Float.toString(start.getYCoord()),
                    Float.toString(end.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());

            break;
          case "ReSize":
            Coordinate initialdim = (Coordinate) animation.getStart();
            Coordinate enddim = (Coordinate) animation.getEnd();
            out += makeAnimationString("width", Float.toString(initialdim.getXCoord()),
                    Float.toString(enddim.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("height", Float.toString(initialdim.getYCoord()),
                    Float.toString(enddim.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());

            break;

          case "ChangeColor":
            Color startc = (Color) animation.getStart();
            Color endc = (Color) animation.getEnd();
            out += makeAnimationString("fill", makeColor(startc), makeColor(endc),
                    animation.getStartTime(), animation.getEndTime());

            break;

          default:
            throw new IllegalArgumentException("Invalid Animation");

        }
      }
    } else if (shape.getClassName().equals("Oval")) {
      for (AbstractAnimation animation : shape.getAnimations()) {
        switch (animation.getClassName()) {

          case "Move":
            Coordinate start = (Coordinate) animation.getStart();
            Coordinate end = (Coordinate) animation.getEnd();
            out += makeAnimationString("cx", Float.toString(start.getXCoord()),
                    Float.toString(end.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("cy", Float.toString(start.getYCoord()),
                    Float.toString(end.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());


            break;
          case "ReSize":
            Coordinate initialdim = (Coordinate) animation.getStart();
            Coordinate enddim = (Coordinate) animation.getEnd();
            out += makeAnimationString("rx", Float.toString(initialdim.getXCoord()),
                    Float.toString(enddim.getXCoord()), animation.getStartTime(),
                    animation.getEndTime());
            out += makeAnimationString("ry", Float.toString(initialdim.getYCoord()),
                    Float.toString(enddim.getYCoord()), animation.getStartTime(),
                    animation.getEndTime());


            break;

          case "ChangeColor":
            Color startc = (Color) animation.getStart();
            Color endc = (Color) animation.getEnd();
            out += makeAnimationString("fill", makeColor(startc), makeColor(endc),
                    animation.getStartTime(), animation.getEndTime());

            break;

          default:
            throw new IllegalArgumentException("Invalid Animation");

        }

      }
    }
    return out;
  }


  /**
   * Create SVG string of animations.
   */
  protected void makeSVG() {
    try {

      String out = "";
      for (AbstractShape shape : this.shapes) {
        Coordinate coord = shape.getPosition();
        if (shape.getClassName().equals("Rectangle") && appears.get(shape.getName())) {
          out += "<rect id =\"" + shape.getName() + "\" " + "x=\"" + coord.getXCoord() + "\" "
                  + "y=\"" + coord.getYCoord() + "\" " +
                  "width=\"" + shape.getWidth() + "\" height =\"" + shape.getHeight() + "\" "
                  + "visibility = \"hidden\"" + " fill = \"" + makeColor(shape.getColor())
                  + "\">\n";
          out += this.makeAnimations(shape);
          if (loop) {
            out += this.svgloop(shape);
          }
          out += "</rect>\n";
        } else if (shape.getClassName().equals("Oval") && appears.get(shape.getName())) {
          float x = coord.getXCoord() + shape.getWidth() / 2;
          float y = coord.getYCoord() + shape.getHeight() / 2;
          out +=
                  "<ellipse id =\"" + shape.getName() + "\" " + "cx=\"" + x + "\" cy=\"" + y + "\" "
                          + "rx=\"" + shape.getWidth() / 2 + "\" ry=\"" + shape.getHeight() / 2
                          + "\" \n" + "visibility = \"hidden\"" + " fill = \""
                          + makeColor(shape.getColor()) + "\">\n";
          out += this.makeAnimations(shape);
          if (loop) {
            out += this.svgloop(shape);
          }
          out += "</ellipse>\n";
        }

      }
      ap.append(out);
    } catch (IOException e) {
      throw new IllegalStateException("Cant append");
    }
  }

  protected void endSVG() {

    String out = "";
    out += "\n</svg>";
    out += "\n\n";
    try {
      ap.append(out);
    } catch (IOException e) {
      throw new IllegalStateException("Cant append");
    }

  }
}