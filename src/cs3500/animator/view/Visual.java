package cs3500.animator.view;


import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;


import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Coordinate;

/**
 * Class for Visual representation of View.
 */
public class Visual extends AbstractView {

  private JFrame mainFrame = new JFrame();

  /**
   * Constructor that calls parent class constructor.
   */
  public Visual() {
    super();
  }

  /**
   * Method to initialize dimensions of shape and appendable to be written to.
   *
   * @param width    width of shape.
   * @param height   height of shape.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;
    DrawAnimation da;

    da = new DrawAnimation(shapes);

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    mainFrame.setTitle("EasyAnimator Visual View");
    mainFrame.setLocation(0, 0);
    mainFrame.setSize((int) screenSize.getWidth(), (int) screenSize.getHeight() - 80);
    mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    mainFrame.add(da);
    mainFrame.setVisible(true);
    da.t.start();
  }

  /**
   * Change shape based on animation.
   * @param s             shape.
   * @param currenttick   current time.
   */
  protected void alterShape(AbstractShape s, int currenttick) {
    if (currenttick > s.getTDissapear()) {
      return;
    }
    for (AbstractAnimation a : s.getAnimations()) {
      if (a.getStartTime() <= currenttick && a.getEndTime() >= currenttick) {
        Coordinate startCoord;
        Coordinate endCoord;
        float x;
        float y;
        switch (a.getClassName()) {
          case "Move":
            startCoord = (Coordinate) a.getStart();
            endCoord = (Coordinate) a.getEnd();

            x = startCoord.getXCoord()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endCoord.getXCoord()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());
            y = startCoord.getYCoord()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endCoord.getYCoord()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());
            s.setMove(new Coordinate(x, y));
            break;

          case "ReSize":
            startCoord = (Coordinate) a.getStart();
            endCoord = (Coordinate) a.getEnd();
            x = startCoord.getXCoord()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endCoord.getXCoord()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());
            y = startCoord.getYCoord()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endCoord.getYCoord()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());
            s.setResize(new Coordinate(x, y));
            break;

          case "ChangeColor":
            Color startc = (Color) a.getStart();
            Color endc = (Color) a.getEnd();
            float red = startc.getRed()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endc.getRed()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());
            float green = startc.getGreen()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endc.getGreen()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());
            float blue = startc.getBlue()
                    * (a.getEndTime() - currenttick) / (a.getEndTime() - a.getStartTime())
                    + endc.getBlue()
                    * (currenttick - a.getStartTime()) / (a.getEndTime() - a.getStartTime());

            s.setColor(new Color((int) red, (int) green, (int) blue));
            break;

          default:
            throw new IllegalArgumentException("Invalid move");
        }
      }
    }


  }

  /**
   * Class to draw the visual animations using action listeners.
   */
  protected class DrawAnimation extends JPanel {

    protected int currenttick = 0;

    private List<AbstractShape> shapes;

    /**
     * Constructor initializing list of shapes.
     *
     * @param shapes list of shapes.
     */
    public DrawAnimation(List<AbstractShape> shapes) {
      this.shapes = shapes;
    }


    /**
     * Swing function to draw the shapes on the screen.
     *
     * @param gr Graphics object.
     */

    public void paintComponent(Graphics gr) {
      Graphics2D g = (Graphics2D) gr;
      super.paintComponent(g);

      for (AbstractShape s : shapes) {
        String type = s.getClassName();
        Coordinate c = s.getPosition();
        Color color = s.getColor();
        int width = (int) s.getWidth();
        int height = (int) s.getHeight();
        switch (type) {
          case "Rectangle":
            g.setColor(color);
            if (currenttick >= s.getTStart() && currenttick <= s.getTDissapear() &&
                    appears.get(s.getName())) {
              g.fillRect((int) c.getXCoord(), (int) c.getYCoord(), width, height);
            }
            break;
          case "Oval":
            g.setColor(color);
            if (currenttick >= s.getTStart() && currenttick <= s.getTDissapear() &&
                    appears.get(s.getName())) {
              g.fillOval((int) c.getXCoord(), (int) c.getYCoord(), width, height);
            }
            break;
          default:
            break;
        }
      }
    }


    /**
     * Method to change values of shape based on animation type and redraw using paintComponent.
     *
     * @param e ActionEvent.
     */
    protected final Timer t = new Timer(500 / tickrate, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        currenttick++;
        for (AbstractShape s : shapes) {
          alterShape(s, currenttick);
        }
        repaint();
      }
    });
  }
}