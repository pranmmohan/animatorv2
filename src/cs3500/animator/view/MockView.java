package cs3500.animator.view;

import java.io.IOException;
import java.util.List;


/**
 * Class to test interactive functionality.
 */
public class MockView extends Visual implements Interactive {

  /**
   * Constructor that calls parent class constructor.
   */
  public MockView() {
    super();
  }

  /**
   * Method to initialize dimensions of appendable to be written to.
   *
   * @param width    width.
   * @param height   height.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;
  }

  @Override
  public void loop() {
    try {
      String out = "Animation is looping\n";
      ap.append(out);
    }
    catch (IOException e) {
      throw new IllegalStateException("Throw error");
    }
  }

  @Override
  public void start() {
    try {
      String out = "Animation has started\n";
      ap.append(out);
    }
    catch (IOException e) {
      throw new IllegalStateException("Throw error");
    }
  }

  @Override
  public void stop() {
    try {
      String out = "Animation has stopped\n";
      ap.append(out);
    }
    catch (IOException e) {
      throw new IllegalStateException("Throw error");
    }
  }


  @Override
  public void restart() {
    try {
      String out = "Animation has restarted\n";
      ap.append(out);
    }
    catch (IOException e) {
      throw new IllegalStateException("Throw error");
    }
  }

  @Override
  public void setAppear(List<String> disappear, List<String> appear) {
    // complex check
  }

  @Override
  public void setSpeed(int speed) {
    try {
      String out = "Speed has changed to: " + speed + "\n";
      ap.append(out);
    }
    catch (IOException e) {
      throw new IllegalStateException("Throw error");
    }
  }

  @Override
  public void exportSVG() {
    // validated using files that were created upon executing jar
  }
}
