package cs3500.animator.view;

import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;
import java.io.IOException;

/**
 * Class for Text view to output the shapes and animations as strings.
 */
public class Textual extends AbstractView {

  /**
   * Constructor, calls constructor of parent class.
   */
  public Textual() {
    super();
  }

  /**
   * Method to initialize dimensions of shape and appendable to be written to.
   * @param width     width of shape.
   * @param height    height of shape.
   * @param tickrate  tickrate.
   * @param ap        appendable.
   */
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;
    this.makeTextual();
  }



  /**
   * Create appendable showing state of shapes and animations performed.
   */
  public void makeTextual() {


    try {
      this.ap.append("Shapes:\n");

      for (AbstractShape shape : shapes) {
        this.ap.append(shape.toString());
        this.ap.append("\n\n");
      }

      for (AbstractAnimation animation : animations) {
        this.ap.append(animation.toString());
        this.ap.append("\n");
      }
    } catch (IOException e) {
      throw new IllegalStateException("Bad state");
    }
  }
}




