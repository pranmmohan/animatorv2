package cs3500.animator.view;


import java.io.IOException;

/**
 * Class to create loop of SVG.
 */
public class SVGloop extends SVGView {


  /**
   * Initialize shape dimensions, tickrate, and appendable.
   *
   * @param width    Width of shape.
   * @param height   height of shape.
   * @param tickrate tickrate.
   * @param ap       appendable.
   */
  @Override
  public void render(int width, int height, int tickrate, Appendable ap) {
    super.width = width;
    super.height = height;
    super.tickrate = tickrate;
    super.ap = ap;
    super.loop = true;
    makeSVGloop();
  }


  /**
   * Create SVG output for looped animation.
   */
  public void makeSVGloop() {
    super.makeHeader();
    makeDummy();
    super.makeSVG();
    super.endSVG();

  }

  /**
   * Default output with no shapes or animations.
   */
  private void makeDummy() {
    String output = "";
    try {
      output += "<rect>\n"
          + "   <animate id=\"base\" begin=\"0;base.end\" dur=\""
          + Float.toString(totalTime / tickrate) + "\" attributeName=\"visibility\" "
              + "from=\"hide\" to=\"hide\"/>\n"
          + "</rect>";
      ap.append(output);
    }
    catch (IOException e) {
      throw new IllegalStateException();
    }
  }
}
