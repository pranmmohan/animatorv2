package cs3500.animator;

import cs3500.animator.controller.AnimationController;
import cs3500.animator.model.Animator;
import cs3500.animator.model.Model;
import cs3500.animator.utils.AnimationFileReader;
import cs3500.animator.utils.TweenModelBuilder;
import cs3500.animator.utils.ViewCreator;
import cs3500.animator.view.View;
import java.awt.Component;
import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import javax.swing.JOptionPane;

/**
 * Class for main function to take user input from command line and perform animations.
 */
public final class EasyAnimator {

  /**
   * Main function.
   * @param args  Arguments for animation, containing flags for the specific visual type and speed.
   */
  public static void main(String[] args) {
    String input = "";
    String type = "";
    boolean iF = false;
    boolean iV = false;
    Integer speed = 1;
    String out = "out";
    Component frame = new Frame();

    for (int i = 0; i < args.length; i += 2) {
      switch (args[i]) {
        case "-if":
          input = args[i + 1];
          iF = true;
          break;
        case "-iv":
          type = args[i + 1];
          iV = true;
          if (type.equals("Textual") && out.isEmpty()) {
            out = "out";
          }
          break;
        case "-o":
          out = args[i + 1];
          break;
        case "-speed":
          speed = Integer.parseInt(args[i + 1]);
          break;
        default:
          JOptionPane.showMessageDialog(frame,
                  "You Entered A Bad Command Line Argument.",
                  "ERROR",
                  JOptionPane.ERROR_MESSAGE);
      }
    }

    if (!iF | !iV) {
      JOptionPane.showMessageDialog(frame,
              "You Have Either not entered a inputfile or a view option",
              "ERROR",
              JOptionPane.ERROR_MESSAGE);
    }

    View view = ViewCreator.create(type);

    TweenModelBuilder builder = new Model.Builder(speed);
    AnimationFileReader reader = new AnimationFileReader();
    OutputStream outWrite = new ByteArrayOutputStream();
    try {
      if (type.equals("hybrid")) {
        Animator model = (Animator) reader.readFile(input, builder);
        view.initialize(model.getShapes(), model.getAnimations());
        AnimationController controller = new AnimationController(model, view, 100000, 100000, speed,
            new PrintStream(outWrite), outWrite);
        controller.run();
      }
      else {
        Animator model = (Animator)reader.readFile(input, builder);
        view.initialize(model.getShapes(), model.getAnimations());
        view.render(100000, 100000, speed, new PrintStream(outWrite));
      }
    }
    catch (IOException e) {
      throw new IllegalStateException("Bad Input");
    }


    if (out.equals("out")) {
      System.out.print(outWrite.toString());
    }

    else if (!out.isEmpty() && !type.equals("hybrid")) {

      try {
        FileWriter fw = new FileWriter(out);
        BufferedWriter toFile = new BufferedWriter(fw);
        toFile.write(outWrite.toString());
        toFile.close();
        fw.close();

      }
      catch (IOException e) {
        throw new IllegalStateException("Cannot create file to write output to");
      }
    }
  }
}
