package cs3500.animator.model;

/**
 * Class that represents the animation of resizing an object.
 */
public class ReSize extends AbstractAnimation {

  String parameter;
  float oldx;
  float oldy;
  float newx;
  float newy;

  /**
   * Constructor initializing Coordinates involved with shape resizing.
   * @param name      name of shape.
   * @param start     start of animation.
   * @param end       end of animation.
   * @param tickrate  tickrate.
   * @param oldx      original X-coord.
   * @param oldy      original Y-coord.
   * @param newx      new X-coord.
   * @param newy      new Y-coord.
   */
  public ReSize(String name, int start, int end, int tickrate, float oldx, float oldy,
      float newx, float newy) {
    super(name, start, end, tickrate);
    this.oldx = oldx;
    this.oldy = oldy;
    this.newx = newx;
    this.newy = newy;
  }

  /**
   * Prints the resizing animation.
   * @return  resize description.
   */
  @Override
  protected String toStringHelper() {
    String output = "scales from Width: " + Float.toString(oldx) + " ,Height: "
        + Float.toString(oldy) + " to Width: " + Float.toString(newx) + ", Height: "
        + Float.toString(newy);
    return output;
  }

  /**
   * Creates copy of Resize animation object to be used in list of animations in View.
   * @return  Resize object.
   */
  @Override
  public AbstractAnimation clone() {
    return new ReSize(this.shapeID, this.tStart, this.tEnd, (int)this.tickrate,
        this.oldx, this.oldy, this.newx, this.newy);
  }

  /**
   * Get animation type.
   * @return  resize.
   */
  @Override
  public String getClassName() {
    return "ReSize";
  }

  /**
   * Get start coordinates.
   * @return  start coordinates.
   */
  @Override
  public Object getStart() {
    return new Coordinate(oldx, oldy);
  }

  /**
   * Get end coordinates.
   * @return  end coordinates.
   */
  @Override
  public Object getEnd() {
    return new Coordinate(newx, newy);
  }
}
