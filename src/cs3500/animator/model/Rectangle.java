package cs3500.animator.model;

import java.awt.Color;

/**
 * Square object. Has a width and a height as its class specific dimensions.
 */
public class Rectangle extends AbstractShape {

  private String placement;


  /**
   * Constructor.
   * @param name        nam of the object.
   * @param color       color.
   * @param position    position.
   * @param width       width of the rectangle.
   * @param height      height of the rectangle.
   */
  public Rectangle(String name, Color color, Coordinate position,
      float tAppear, float tDissapear, double tickrate, float width, float height) {
    super(name, color, position, tAppear, tDissapear, tickrate, width, height);

    if (width < 0 || height < 0) {
      throw new IllegalArgumentException("Width and height of rectangle must be greater than 0");
    }

    this.width = width;
    this.height = height;
    this.placement = "Lower-left corner";

  }

  /**
   * Method to resize Rectangle.
   * @param set New coordinates of width and height.
   */
  @Override
  public void setResize(Coordinate set) {
    this.width = set.getXCoord();
    this.height = set.getYCoord();

  }

  /**
   * Get position of Rectangle.
   * @return  position.
   */
  @Override
  public String getPlacement() {
    return this.placement;
  }

  /**
   * Gets the class name for the models hashmap.
   * @return the class name.
   */
  @Override
  public String getClassName() {
    return "Rectangle";
  }

  /**
   * Gets the dimension of the square in the print format.
   * @return the dimension of the square
   */
  @Override
  public float getSpecificDim(String dimtype) {
    if (dimtype.equals("Width")) {
      return this.width;

    }
    else if (dimtype.equals("Height")) {
      return this.height;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Sets the dimensions for a move operation.
   * @param dimtype the dimension that will be changed
   * @param dim     the value the dimension will be changed to
   */
  @Override
  public void setDim(String dimtype, float dim) {
    if (dimtype.equals("Width")) {
      this.width = dim;

    }
    else if (dimtype.equals("Height")) {
      this.height = dim;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Prints the wight and height of the rectangle.
   * @return the dimensions of teh rectangle.
   */
  @Override
  public String printDimensions() {
    String output = "Width: " + Float.toString(width) + ", Height: "
        + Float.toString(height);
    return output;
  }

  /**
   * Create copy of Rectangle, to pass to list in View.
   * @return  Rectangle.
   */
  @Override
  public AbstractShape clone() {
    AbstractShape rectangle = new Rectangle(this.name, this.color, this.position,
        this.tAppear, this.tDisappear, this.tickrate, this.width, this.height);
    rectangle.animations = this.cloneAnimations();
    return rectangle;
  }

  /**
   * Get width of Rectangle.
   * @return  width.
   */
  @Override
  public float getWidth() {
    return this.width;
  }

  /**
   * Get height of Rectangle.
   * @return  height.
   */
  @Override
  public float getHeight() {
    return this.height;
  }

  @Override
  public Coordinate getPosition() {
    return this.position;
  }
}
