package cs3500.animator.model;

import java.awt.Color;

/**
 * Animation class to change color of a shape.
 */
public class ChangeColor extends AbstractAnimation {

  Color endColor;
  Color startColor;


  /**
   * Constructor for setting a shape's color.
   *
   * @param name       name of shape.
   * @param start      start of animation.
   * @param end        end of animation.
   * @param startColor starting color.
   * @param endColor   ending color.
   */
  public ChangeColor(String name, int start, int end, int tickrate, Color startColor,
                     Color endColor) {
    super(name, start, end, tickrate);
    this.startColor = startColor;
    this.endColor = endColor;
  }

  /**
   * Get color.
   *
   * @param color Java awt color.
   * @return Color as string.
   */
  private String getColorasString(Color color) {
    return "(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ")";
  }

  /**
   * Print color change.
   *
   * @return color change.
   */
  @Override
  public String toStringHelper() {
    return "changes color from " + getColorasString(startColor) + " to "
            + getColorasString(endColor);
  }

  /**
   * Copy the change color animation to use in view.
   *
   * @return copy of change color animation.
   */
  @Override
  public AbstractAnimation clone() {
    return new ChangeColor(this.shapeID, this.tStart, this.tEnd, (int) this.tickrate,
            this.startColor, this.endColor);
  }

  /**
   * Get type of animation.
   *
   * @return Change color.
   */
  @Override
  public String getClassName() {
    return "ChangeColor";
  }

  /**
   * Get start color.
   *
   * @return start color.
   */
  @Override
  public Object getStart() {
    return new Color(startColor.getRed(), startColor.getGreen(), startColor.getBlue());
  }

  /**
   * Get end color.
   *
   * @return end color.
   */
  @Override
  public Object getEnd() {
    return new Color(endColor.getRed(), endColor.getGreen(), endColor.getBlue());
  }
}
