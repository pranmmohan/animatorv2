package cs3500.animator.model;

import java.awt.Color;

/**
 * Oval class. It is a shape.
 */
public class Oval extends AbstractShape {

  private String placement = "Center";


  /**
   * Constructor.
   * @param name        name of the shape.
   * @param color       color.
   * @param position    position.
   * @param width     x radius.
   * @param height     y radius.
   */
  public Oval(String name, Color color, Coordinate position,
      float tAppear, float tDissapear, double tickrate, float width, float height) {
    super(name, color, position, tAppear, tDissapear, tickrate, width, height);

    if (width < 0 || height < 0) {
      throw new IllegalArgumentException("Radii for oval must be positive");
    }

    this.width = width;
    this.height = height;
  }

  /**
   * Method to change coordinates of Oval to resize it.
   * @param set New coordinate.
   */
  @Override
  public void setResize(Coordinate set) {
    this.width = set.getXCoord();
    this.height = set.getYCoord();

  }

  /**
   * Get position.
   * @return  position.
   */
  @Override
  public String getPlacement() {
    return this.placement;
  }

  /**
   * Create copy of Oval, to pass to list in View.
   * @return  Oval.
   */
  @Override
  public AbstractShape clone() {
    AbstractShape oval = new Oval(this.name, this.color, this.position,
        this.tAppear, this.tDisappear, this.tickrate, this.width, this.height);
    oval.animations = this.cloneAnimations();
    return oval;
  }

  /**
   * Prints the class name.
   * @return the class name.
   */
  @Override
  public String getClassName() {
    return "Oval";
  }

  /**
   * Prints the dimension of the shape.
   * @return the dimension of the shape.
   */
  @Override
  public float getSpecificDim(String dimtype) {
    if (dimtype.equals("X")) {
      return this.width;

    }
    else if (dimtype.equals("Y")) {
      return this.height;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Sets one of the dimensions to the given value.
   * @param dimtype the dimension to be changed
   * @param dim the value the dimension is going to be changed to.
   */
  @Override
  public void setDim(String dimtype, float dim) {
    if (dimtype.equals("X Radius")) {
      this.width = dim;

    }
    else if (dimtype.equals("Y Radius")) {
      this.height = dim;
    }

    else {
      throw new IllegalArgumentException("Entered Invalid Field");
    }
  }

  /**
   * Prints the dimensions of the oval its xradius and yradius.cannersca
   * @return the dimensions in string format
   */
  @Override
  public String printDimensions() {
    String output = "X Radius: " + Float.toString(width) + ", Y Radius: "
        + Float.toString(height);
    return output;
  }

  /**
   * Get width of Oval.
   * @return width.
   */
  @Override
  public float getWidth() {
    return 2 * this.width;
  }

  /**
   * Get height of Oval.
   * @return height.
   */
  @Override
  public float getHeight() {
    return 2 * this.height;
  }

  @Override
  public Coordinate getPosition() {
    return new Coordinate(this.position.getXCoord() - width, this.position.getYCoord() - height);
  }

}
