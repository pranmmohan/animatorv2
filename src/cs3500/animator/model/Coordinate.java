package cs3500.animator.model;

/**
 * Class to create a coordinate pair to simplify representing position of a shape.
 */
public class Coordinate {

  // x and y coordinates
  private float x;
  private float y;

  /**
   * Constructor initializing coordinates.
   *
   * @param x X-coordinate.
   * @param y Y-coordinate.
   */
  public Coordinate(float x, float y) {

    this.x = x;
    this.y = y;
  }

  /**
   * Get the X-coordinate.
   *
   * @return Coordinate of X.
   */
  public float getXCoord() {
    return this.x;
  }

  /**
   * Get the Y-coordinate.
   *
   * @return Coordinate of Y.
   */
  public float getYCoord() {
    return this.y;
  }

  /**
   * Print out the coordinates.
   * @return coordinates.
   */
  @Override
  public String toString() {
    return "(" + Float.toString(x) + "," + Float.toString(y) + ")";

  }
}