package cs3500.animator.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * AbstractShape class for all shapes, since all shapes share common properties.
 */
public abstract class AbstractShape {

  protected String name;
  protected Color color;
  protected Coordinate position;
  protected float tAppear;
  protected float tDisappear;
  protected float height;
  protected float width;
  protected PriorityQueue<AbstractAnimation> animations;
  protected double tickrate;
  protected List<AbstractAnimation> cloneAnimations;

  /**
   * Super constructor.
   *
   * @param name     name of the shape.
   * @param color    color of the shape.
   * @param position position of the shape.
   */
  public AbstractShape(String name, Color color, Coordinate position,
                       float tAppear, float tDisappear, double tickrate, float width,
                       float height) {
    if (tDisappear < tAppear) {
      throw new IllegalArgumentException("Shape dissapears before it appears");
    }
    if (tAppear < 0 || tDisappear < 0) {
      throw new IllegalArgumentException("Appear and disappear times must be greater than 0");
    }

    this.name = name;
    this.color = color;
    this.position = position;
    this.tAppear = 0;
    this.tDisappear = 0;
    this.tAppear = tAppear;
    this.tDisappear = tDisappear;
    this.animations = new PriorityQueue<>(new AnimationComparator());
    this.tickrate = tickrate;
    this.cloneAnimations = new ArrayList<>();
    this.width = width;
    this.height = height;
  }

  /**
   * Method to create a copy of the shape to use in the view.
   *
   * @return copy of shape.
   */
  public  abstract AbstractShape clone();

  /**
   * Get the placement(lower-left for square, center at for oval) of the shape.
   *
   * @return placement of the shape.
   */
  protected abstract String getPlacement();

  /**
   * Prints the shape's details.
   *
   * @return the shape.
   */
  @Override
  public String toString() {
    String output = "Name: " + this.name + "\n"
            + "Type: " + this.getClassName() + "\n"
            + this.getPlacement() + ": " + this.printcoords() + ", " + printDimensions()
            + ", Color: " + printcolors() + "\n"
            + "Appears at t=" + Double.toString(tAppear / tickrate) + "s\n"
            + "Disappears at t=" + Double.toString(tDisappear / tickrate) + "s";

    return output;
  }


  /**
   * Prints the coordinates of the shape.
   *
   * @return coordinates
   */
  public String printcoords() {
    return "(" + this.position.getXCoord() + "," + this.position.getYCoord() + ")";
  }

  public float getTStart() {
    return this.tAppear;
  }

  /**
   * Prints the colors of the shape.
   *
   * @return colors in (r,g,b) format
   */
  public String printcolors() {
    return "("
            + Integer.toString(color.getRed()) + ","
            + Integer.toString(color.getGreen()) + ","
            + Integer.toString(color.getBlue()) + ")";
  }

  /**
   * Gets the name of the shape.
   *
   * @return name of the shape
   */
  public abstract String getClassName();


  /**
   * Gets the unique dimensions of the shape.
   *
   * @return the dimensions of the shape.
   */
  public abstract float getSpecificDim(String dimtype);

  /**
   * Gets the position of the shape.
   *
   * @return the original position of the shape.
   */
  public abstract Coordinate getPosition();

  /**
   * Gets the color of the shape.
   *
   * @return the color of the shape.
   */
  public Color getColor() {
    return this.color;
  }

  /**
   * Gets the name of the name of the shape.
   *
   * @return name of the shape
   */
  public String getName() {
    return this.name;
  }


  public float getTDissapear() {
    return this.tDisappear;
  }

  /**
   * Sets the color of the shape.
   *
   * @param color new color of the shape.
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * Sets the new position of the shape.
   *
   * @param coordinate Coordinate of the shape.
   */
  public void setMove(Coordinate coordinate) {
    this.position = coordinate;
  }

  /**
   * Sets new size of shape.
   *
   * @param coordinate destination coordinate.
   */
  public abstract void setResize(Coordinate coordinate);


  /**
   * Sets the new dimension of the shape.
   *
   * @param dimtype the dimension to be changed.
   * @param dim     the value the dimension is going to be changed to.
   */
  public abstract void setDim(String dimtype, float dim);


  /**
   * Prints the dimensions of the shape.
   *
   * @return the dimensions of the shape.
   */
  public abstract String printDimensions();

  /**
   * Checks if the time to move the shape is valid.
   *
   * @param tFrom time the shape starts moving at.
   * @param tTo   time the shape starts moving till.
   * @return valid times.
   */
  public boolean validTime(int tFrom, int tTo) {
    return this.tAppear <= tFrom & this.tDisappear >= tTo;
  }

  /**
   * Create a priority queue for list of animations based on start times.
   *
   * @return PQ of animations.
   */
  protected PriorityQueue<AbstractAnimation> cloneAnimations() {
    PriorityQueue<AbstractAnimation> toReturn = new PriorityQueue<>(new AnimationComparator());
    for (AbstractAnimation animation : this.animations) {
      toReturn.add(animation.clone());
    }
    return toReturn;
  }

  /**
   * Get list of animations - used in View to get from Model.
   *
   * @return list of animations.
   */
  public List<AbstractAnimation> getAnimations() {
    List<AbstractAnimation> toReturn = new ArrayList<>();
    if (!this.animations.isEmpty()) {
      while (!this.animations.isEmpty()) {
        AbstractAnimation tempAnimation  = this.animations.poll();
        toReturn.add(tempAnimation.clone());
        this.cloneAnimations.add(tempAnimation.clone());

      }
    }
    else {
      for (int i = 0; i < cloneAnimations.size(); i++) {
        toReturn.add(cloneAnimations.get(i).clone());
      }

    }

    return toReturn;
  }

  /**
   * Get width of shape.
   *
   * @return width.
   */
  public abstract float getWidth();

  /**
   * Get height of shape.
   *
   * @return height.
   */
  public abstract float getHeight();

  /**
   * Add animation to list of animation.
   *
   * @param animation animation.
   */
  public void addAnimation(AbstractAnimation animation) {
    if (validTime(animation.tStart, animation.tEnd)) {
      this.animations.add(animation);
    } else {
      throw new IllegalArgumentException("Animation must occur during start and end time "
              + "of the shape appearing.");
    }
  }

  /**
   * Create copy of shape.
   * @param shape   shape.
   */
  public void copy(AbstractShape shape) {
    this.width = shape.width;
    this.height = shape.height;
    Color color = new Color(shape.color.getRed(), shape.color.getGreen(), shape.color.getBlue());
    Coordinate coord = new Coordinate(shape.position.getXCoord(), shape.position.getYCoord());
    this.color = color;
    this.position = coord;
  }



}