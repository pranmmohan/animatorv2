package cs3500.animator.model;

import java.util.Comparator;

/**
 * Class to compare animations to check for same type of overlapping animations and compare start
 * times of animations.
 */

class AnimationComparator implements Comparator<AbstractAnimation> {

  @Override
  public int compare(AbstractAnimation a, AbstractAnimation b) {

    if (AbstractAnimation.overlappingCommand(a, b)) {
      throw new IllegalArgumentException("You entered an illegal command");
    }

    if (a.getStartTime() >= b.getStartTime()) {
      return 1;
    } else {
      return -1;
    }
  }
}

