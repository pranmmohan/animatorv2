package cs3500.animator.model;

/**
 * Class that represents the animation of moving an object.
 */
public class Move extends AbstractAnimation {

  Coordinate start;
  Coordinate end;

  /**
   * Constructor for Move, initializes start and enc coordinates.
   *
   * @param name     Name of the shape.
   * @param start    Appear time.
   * @param end      Disappear time.
   * @param tickrate Tick rate.
   * @param moveFrom Source coordinate.
   * @param moveTo   Destination coordinate.
   */
  public Move(String name, int start, int end, int tickrate, Coordinate moveFrom,
              Coordinate moveTo) {
    super(name, start, end, tickrate);
    this.start = moveFrom;
    this.end = moveTo;
  }

  /**
   * Print the move.
   *
   * @return move.
   */
  @Override
  protected String toStringHelper() {
    String output = "move from " + start.toString() + " to " + end.toString();
    return output;
  }

  /**
   * Create copy of the move animation to be used in View.
   *
   * @return Move object.
   */
  @Override
  public AbstractAnimation clone() {
    return new Move(this.shapeID, this.tStart, this.tEnd, (int) this.tickrate,
            this.start, this.end);
  }

  /**
   * Get animation type.
   *
   * @return Move.
   */
  @Override
  public String getClassName() {
    return "Move";
  }

  /**
   * Get start coordinate.
   *
   * @return coordinate.
   */
  @Override
  public Object getStart() {
    return new Coordinate(start.getXCoord(), start.getYCoord());
  }

  /**
   * Get destination coordinate.
   *
   * @return coordinate.
   */
  @Override
  public Object getEnd() {
    return new Coordinate(end.getXCoord(), end.getYCoord());
  }
}