
import static org.junit.Assert.assertEquals;

import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.ChangeColor;
import java.awt.Color;
import org.junit.Test;

public class ChangeColorTest {

  ChangeColor color1 = new ChangeColor("a",4, 8, 8,
      new Color(1,2,3,4), new Color(1,3,4));

  ChangeColor color2 = new ChangeColor("a",5, 8, 8,
      new Color(1,3,5), new Color(1,6,8));

  ChangeColor color3 = new ChangeColor("a",1, 2, 8,
      new Color(1,2,5), new Color(5, 2, 3));

  @Test
  public void InvalidOverlap() {
    assertEquals(true, AbstractAnimation.overlappingCommand(color1, color2));
  }

  @Test
  public void validOverlap() {
    assertEquals(false, AbstractAnimation.overlappingCommand(color2, color3));

  }

}