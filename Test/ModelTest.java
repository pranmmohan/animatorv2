import static org.junit.Assert.assertEquals;
import cs3500.animator.model.Animator;
import cs3500.animator.model.Model;
import cs3500.animator.utils.TweenModelBuilder;
import org.junit.Test;

public class ModelTest {

  TweenModelBuilder builder;

  /**
   * Test printing shapes and animations with no overlap between animations of different shapes.
   */
  @Test
  public void noOverlap() {
    String output = "Shapes: \n"
            + "Name: O\n"
            + "Type: Oval\n"
            + "Center: (60.0,10.0), X Radius: 30.0, Y Radius: 90.0, Color: (26,77,255)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=40.0sShape O move from (70.0,70.0) to (400.0,400.0) from t=20.0 to "
            + "t=30.0\n"
            + "Shapes: \n"
            + "Name: R\n"
            + "Type: Rectangle\n"
            + "Lower-left corner: (10.0,10.0), Width: 30.0, Height: 20.0, Color: (51,51,26)\n"
            + "Appears at t=0.0s\n"
            + "Disappears at t=30.0sShape R move from (20.0,20.0) to (70.0,70.0) from t=8.0 to "
            + "t=15.0\n";
    builder = new Model.Builder(1);

    builder.addOval("O",60, 10, 30, 90, 0.1f,
        0.3f,1.0f, 10, 40 );
    builder.addRectangle("R", 10, 10, 30, 20, 0.2f,
        0.2f, 0.1f, 0, 30);
    builder.addMove("R", 20,20,70,70,8,
            15);
    builder.addMove("O", 70,70,400,400,20,
            30);
    Animator model = (Animator) builder.build();

    assertEquals(output, model.printShape("O") + model.printShape("R"));
  }

  /**
   * Test multi move functionality is printing correctly.
   */
  @Test
  public void testMultiMoveConsecutiveTime() {
    String output = "Shapes: \n"
            + "Name: O\n"
            + "Type: Oval\n"
            + "Center: (60.0,10.0), X Radius: 30.0, Y Radius: 90.0, Color: (26,77,255)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=40.0sShape O move from (20.0,20.0) to (70.0,70.0) from t=12.0 to "
            + "t=15.0\n"
            + "Shape O move from (70.0,70.0) to (400.0,400.0) from t=20.0 to t=30.0\n";
    builder = new Model.Builder(1);

    builder.addOval("O",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 10, 40 );
    builder.addMove("O", 20,20,70,70,12,
            15);
    builder.addMove("O", 70,70,400,400,20,
            30);
    Animator model = (Animator) builder.build();

    assertEquals(output, model.printShape("O"));
  }

  /**
   * Test moving prior to shape appear time.
   */
  @Test (expected = IllegalArgumentException.class)
  public void tesMoveBeforeAppear() {
    builder = new Model.Builder(1);

    builder.addOval("O",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 10, 40 );
    builder.addMove("O", 20,20,70,70,8,
            15);
    Animator model = (Animator) builder.build();
  }

  /**
   * Test multi move functionality is printing correctly with move made during first move.
   */
  @Test (expected = IllegalArgumentException.class)
  public void testMultiMoveOverlapTime() {
    builder = new Model.Builder(1);

    builder.addRectangle("R",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 10, 40 );
    builder.addMove("R", 20,20,70,70,8,
            15);
    builder.addMove("R", 30,70,10,15,10,
            12);
    Animator model = (Animator) builder.build();
  }

  /**
   * Test invalid end time (earlier than start time).
   */
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidEndTime() {
    builder = new Model.Builder(1);

    builder.addRectangle("R",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 5, 1);
    Animator model = (Animator) builder.build();
  }

  /**
   * Test changing color.
   */
  @Test
  public void testChangeColor() {
    String output = "Shapes: \n"
            + "Name: R\n"
            + "Type: Rectangle\n"
            + "Lower-left corner: (60.0,10.0), Width: 30.0, Height: 90.0, Color: (26,77,255)\n"
            + "Appears at t=0.0s\n"
            + "Disappears at t=20.0sShape R changes color from (26,102,230) to (77,128,51) from "
            + "t=2.0 to t=10.0\n";
    builder = new Model.Builder(1);

    builder.addRectangle("R",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 0, 20);
    builder.addColorChange("R",0.1f,0.4f,0.9f,0.3f,0.5f,
            0.2f,2,10);
    Animator model = (Animator) builder.build();

    assertEquals(output, model.printShape("R"));
  }

  /**
   * Test resizing oval and rectangle.
   */
  @Test
  public void testResize() {
    String output = "Shapes: \n"
            + "Name: O\n"
            + "Type: Oval\n"
            + "Center: (60.0,10.0), X Radius: 30.0, Y Radius: 90.0, Color: (26,77,255)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=40.0sShape O scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, "
            + "Height: 40.0 from t=12.0 to t=20.0\n"
            + "Shapes: \n"
            + "Name: R\n"
            + "Type: Rectangle\n"
            + "Lower-left corner: (10.0,10.0), Width: 30.0, Height: 20.0, Color: (51,51,26)\n"
            + "Appears at t=0.0s\n"
            + "Disappears at t=30.0sShape R scales from Width: 1.0 ,Height: 10.0 to Width: 6.0, "
            + "Height: 30.0 from t=15.0 to t=17.0\n";
    builder = new Model.Builder(1);

    builder.addOval("O",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 10, 40 );
    builder.addRectangle("R", 10, 10, 30, 20, 0.2f,
            0.2f, 0.1f, 0, 30);
    builder.addScaleToChange("O",10,17,20,40,12,
            20);
    builder.addScaleToChange("R",1,10,6,30,15,
            17);
    Animator model = (Animator) builder.build();

    assertEquals(output, model.printShape("O") + model.printShape("R"));
  }

  /**
   * Test moving shape after it disappears.
   */
  @Test(expected = IllegalArgumentException.class)
  public void testMoveAfterDisappear() {
    builder = new Model.Builder(1);

    builder.addOval("O",60, 10, 30, 90, 0.1f,
            0.3f,1.0f, 10, 40 );
    builder.addMove("O", 20,20,70,70,41,
            50);

    Animator model = (Animator) builder.build();
  }

  /**
   * Test creating shape with invalid dimension.
   */
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidLength() {
    builder = new Model.Builder(1);

    builder.addOval("O",12, 10, -2, 90, 0.1f,
            0.3f,1.0f, 10, 40 );
    builder.addRectangle("R", 5, 10, -5, 20, 0.2f,
            0.2f, 0.1f, 0, 30);

    Animator model = (Animator) builder.build();
  }

  /**
   * Empty animation.
   */
  @Test
  public void emptyAnimation() {
    String output = "Shapes: \n"
            + "Name: O\n"
            + "Type: Oval\n"
            + "Center: (12.0,10.0), X Radius: 10.0, Y Radius: 90.0, Color: (26,77,255)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=40.0s";
    builder = new Model.Builder(1);

    builder.addOval("O",12, 10, 10, 90, 0.1f,
            0.3f,1.0f, 10, 40 );

    Animator model = (Animator) builder.build();

    assertEquals(output, model.printShape("O"));

  }

  /**
   * Test changing color of a Oval while moving it.
   */
  @Test
  public void changeColorWhileMoving() {
    String output = "Shapes: \n"
            + "Name: O1\n"
            + "Type: Oval\n"
            + "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=50.0sShape O1 move from (60.0,60.0) to (100.0,100.0) from t=17.0 to "
            + "t=30.0\n"
            + "Shape O1 changes color from (77,179,51) to (26,77,179) from t=20.0 to t=25.0\n";
    builder = new Model.Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,20,25);
    Animator model = (Animator)builder.build();

    assertEquals(output, model.printShape("O1"));
  }
}