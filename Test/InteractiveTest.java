import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;

import cs3500.animator.controller.AnimationController;
import cs3500.animator.model.Animator;
import cs3500.animator.model.Model;
import cs3500.animator.utils.AnimationFileReader;
import cs3500.animator.utils.TweenModelBuilder;
import cs3500.animator.view.MockView;
import cs3500.animator.view.View;

import static org.junit.Assert.assertEquals;

/**
 * Tests the hybrid view and controller.
 */
public class InteractiveTest {

  /**
   * Testing interactivity in hybrid view using controller.
   */
  @Test
  public void testController() {
    String output = "Animation has started\n"
            + "Animation is looping\n"
            + "Animation has stopped\n"
            + "Speed has changed to: 10\n";
    String file = "/Users/Sean/Ju/Northeastern/Senior/OOD/projects/Animatorv2/out/artifacts/"
            + "Animatorv2_jar/smalldemo.txt";
    int speed = 10;
    View view = new MockView();
    Animator model = null;

    TweenModelBuilder builder = new Model.Builder(speed);
    AnimationFileReader reader = new AnimationFileReader();
    OutputStream outWrite = new ByteArrayOutputStream();
    Appendable ap = new PrintStream(outWrite);

    try {
      model = (Animator) reader.readFile(file, builder);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    view.initialize(model.getShapes(), model.getAnimations());
    AnimationController controller = new AnimationController(model, view, 100000, 100000, speed,
            ap, outWrite);
    controller.run();
    controller.start();
    controller.loop();
    controller.pause();
    controller.setSpeed(10);


    assertEquals(output, outWrite.toString());










  }


} // end of test class
