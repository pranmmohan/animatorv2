import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import cs3500.animator.utils.TweenModelBuilder;
import cs3500.animator.model.AbstractAnimation;
import cs3500.animator.model.AbstractShape;
import cs3500.animator.model.Animator;
import cs3500.animator.model.Model.Builder;
import cs3500.animator.view.Textual;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import org.junit.Test;

public class TextualTest {


  Textual textual;
  TweenModelBuilder builder;
  private OutputStream out = new ByteArrayOutputStream();

  Textual textual2;
  TweenModelBuilder builder2;
  OutputStream out2 = new ByteArrayOutputStream();

  /**
   * Basic test creating 2 shapes and making a move on one of them.
   */
  @Test
  public void SimpleExample() {
    String output = "Shapes:\n"
            + "Name: S\n"
            + "Type: Oval\n"
            + "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=50.0s\n"
            + "\n"
            + "Name: G\n"
            + "Type: Rectangle\n"
            + "Lower-left corner: (50.0,50.0), Width: 14.0, Height: 14.0, Color: (51,51,26)\n"
            + "Appears at t=2.0s\n"
            + "Disappears at t=4.0s\n"
            + "\n"
            + "Shape S move from (40.0,30.0) to (60.0,60.0) from t=11.0 to t=20.0\n";
    builder = new Builder(1);
    builder.addOval("S",40, 30, 5, 10, 0.3f,
            0.3f,0.2f, 10, 50 );
    builder.addRectangle("G", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 2, 4);
    builder.addMove("S",40,30,60,60,11,
            20);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output,out.toString());
  }

  /**
   * Test passing in no shapes.
   */
  @Test
  public void noShapes() {
    String output = "Shapes:\n";

    builder = new Builder(1);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output,out.toString());
  }

  /**
   *  Check that changing the tick rate results in different appear and disappear time.
   */
  @Test
  public void changeTickRate() {
    builder = new Builder(2);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);

    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    builder2 = new Builder(3);
    builder2.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder2.addMove("O1",60,60,100,100,17,
            30);

    Animator model2 = (Animator)builder2.build();

    List<AbstractShape> shapes2 = model2.getShapes();
    List<AbstractAnimation> animations2 = model2.getAnimations();

    textual2 = new Textual();
    textual2.initialize(shapes2, animations2);
    textual2.render(0,0,1,new PrintStream(out2));

    assertNotEquals(out.toString(),out2.toString());
  }

  /**
   * Test passing in empty animation.
   */
  @Test
  public void noAnimations() {
    String output = "Shapes:\n" +
            "Name: R1\n" +
            "Type: Rectangle\n" +
            "Lower-left corner: (50.0,50.0), Width: 14.0, Height: 14.0, Color: (51,51,26)\n" +
            "Appears at t=5.0s\n" +
            "Disappears at t=10.0s\n" +
            "\n";

    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 5, 10);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output,out.toString());
  }

  /**
   * Test for invalid start and end times.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAppearTime() {
    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, -5, -1);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }

  /**
   * Passing invalid time for animation times.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAnimationTime() {
    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 10, 20);
    builder.addMove("R1",60,60,100,100,-4,
            -1);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }


  /**
   * Specify an animation that is invalid - occurs before the shape appears.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAnimation() {
    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 10, 20);
    builder.addMove("R1",60,60,100,100,8,
            13);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }

  /**
   * Specify an animation that is invalid - occurs after the shape disappears.
   */
  @Test (expected = IllegalArgumentException.class)
  public void invalidAnimation2() {
    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 10, 20);
    builder.addMove("R1",60,60,100,100,25,
            30);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }

  /**
   * Test moving a Rectangle during a move animation that is already occuring.
   */
  @Test (expected = IllegalArgumentException.class)
  public void moveAnimationOverlap() {
    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 0, 20);
    builder.addMove("R1",60,60,100,100,3,
            15);
    builder.addMove("R1",10,10,17,30,7,
            9);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }

  /**
   * Test changing color of a Rectangle during an existing color change animation for that
   * Rectangle.
   */
  @Test (expected = IllegalArgumentException.class)
  public void changeColorOverlap() {
    builder = new Builder(1);
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 0, 20);
    builder.addColorChange("R1",0.1f,0.4f,0.9f,0.3f,0.5f,
            0.2f,2,10);
    builder.addColorChange("R1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,5,8);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }

  /**
   * Test changing size of a Oval during an existing size change animation for it.
   */
  @Test (expected = IllegalArgumentException.class)
  public void reSizeOverlap() {
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addScaleToChange("O1",10,17,20,40,12,
            20);
    builder.addScaleToChange("O1",1,10,6,30,15,
            17);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));
  }

  /**
   * Test changing color of a Oval while moving it.
   */
  @Test
  public void changeColorWhileMoving() {
    String output = "Shapes:\n" +
            "Name: O1\n" +
            "Type: Oval\n" +
            "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n" +
            "Appears at t=10.0s\n" +
            "Disappears at t=50.0s\n" +
            "\n" +
            "Shape O1 move from (60.0,60.0) to (100.0,100.0) from t=17.0 to t=30.0\n" +
            "Shape O1 changes color from (77,179,51) to (26,77,179) from t=20.0 to t=25.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,20,25);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test changing color of a Oval while resizing it.
   */
  @Test
  public void changeColorWhileResizing() {
    String output = "Shapes:\n" +
            "Name: O1\n" +
            "Type: Oval\n" +
            "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n" +
            "Appears at t=10.0s\n" +
            "Disappears at t=50.0s\n" +
            "\n" +
            "Shape O1 scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, Height: 40.0 from " +
            "t=12.0 to t=20.0\n" +
            "Shape O1 changes color from (77,179,51) to (26,77,179) from t=14.0 to t=17.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addScaleToChange("O1",10,17,20,40,12,
            20);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,14,17);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test changing size of a Oval while moving it.
   */
  @Test
  public void reSizeWhileMoving() {
    String output = "Shapes:\n" +
            "Name: O1\n" +
            "Type: Oval\n" +
            "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n" +
            "Appears at t=10.0s\n" +
            "Disappears at t=50.0s\n" +
            "\n" +
            "Shape O1 move from (60.0,60.0) to (100.0,100.0) from t=17.0 to t=30.0\n" +
            "Shape O1 scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, Height: 40.0 from " +
            "t=20.0 to t=23.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addMove("O1",60,60,100,100,17,
            30);
    builder.addScaleToChange("O1",10,17,20,40,20,
            23);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test resize Oval while changing color.
   */
  @Test
  public void resizeWhileChangeColor() {
    String output = "Shapes:\n" +
            "Name: O1\n" +
            "Type: Oval\n" +
            "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n" +
            "Appears at t=10.0s\n" +
            "Disappears at t=50.0s\n" +
            "\n" +
            "Shape O1 changes color from (77,179,51) to (26,77,179) from t=17.0 to t=24.0\n" +
            "Shape O1 scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, Height: 40.0 from " +
            "t=20.0 to t=23.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,17,24);
    builder.addScaleToChange("O1",10,17,20,40,20,
            23);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test moving Oval while changing color.
   */
  @Test
  public void moveWhileChangeColor() {
    String output = "Shapes:\n" +
            "Name: O1\n" +
            "Type: Oval\n" +
            "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n" +
            "Appears at t=10.0s\n" +
            "Disappears at t=50.0s\n" +
            "\n" +
            "Shape O1 changes color from (77,179,51) to (26,77,179) from t=17.0 to t=24.0\n" +
            "Shape O1 move from (60.0,60.0) to (100.0,100.0) from t=20.0 to t=23.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,17,24);
    builder.addMove("O1",60,60,100,100,20,
            23);
    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test moving Oval while resizing.
   */
  @Test
  public void moveWhileResizing() {
    String output = "Shapes:\n" +
            "Name: O1\n" +
            "Type: Oval\n" +
            "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n" +
            "Appears at t=10.0s\n" +
            "Disappears at t=50.0s\n" +
            "\n" +
            "Shape O1 scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, Height: 40.0 from " +
            "t=17.0 to t=30.0\n" +
            "Shape O1 move from (60.0,60.0) to (100.0,100.0) from t=20.0 to t=23.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addScaleToChange("O1",10,17,20,40,17,
            30);
    builder.addMove("O1",60,60,100,100,20,
            23);

    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }

  /**
   * Test multiple consecutive moves with overlap(of different animations) and multiple shapes.
   */
  @Test
  public void multipleMoves() {
    String output = "Shapes:\n"
            + "Name: O1\n"
            + "Type: Oval\n"
            + "Center: (40.0,30.0), X Radius: 5.0, Y Radius: 10.0, Color: (77,77,51)\n"
            + "Appears at t=10.0s\n"
            + "Disappears at t=50.0s\n"
            + "\n"
            + "Name: R1\n"
            + "Type: Rectangle\n"
            + "Lower-left corner: (50.0,50.0), Width: 14.0, Height: 14.0, Color: (51,51,26)\n"
            + "Appears at t=0.0s\n"
            + "Disappears at t=30.0s\n"
            + "\n"
            + "Shape R1 move from (40.0,30.0) to (60.0,60.0) from t=2.0 to t=10.0\n"
            + "Shape R1 move from (10.0,20.0) to (200.0,100.0) from t=12.0 to t=15.0\n"
            + "Shape R1 changes color from (77,179,51) to (26,77,179) from t=13.0 to t=14.0\n"
            + "Shape R1 scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, Height: 40.0 from "
            + "t=17.0 to t=30.0\n"
            + "Shape O1 scales from Width: 10.0 ,Height: 17.0 to Width: 20.0, Height: 40.0 from "
            + "t=17.0 to t=30.0\n"
            + "Shape O1 move from (60.0,60.0) to (100.0,100.0) from t=20.0 to t=23.0\n"
            + "Shape O1 changes color from (77,179,51) to (26,77,179) from t=24.0 to t=27.0\n"
            + "Shape O1 move from (10.0,20.0) to (200.0,100.0) from t=30.0 to t=35.0\n";
    builder = new Builder(1);
    builder.addOval("O1",40, 30, 5, 10, 0.3f, 0.3f,
            0.2f, 10, 50 );
    builder.addRectangle("R1", 50, 50, 14, 14, 0.2f,
            0.2f, 0.1f, 0, 30);
    builder.addMove("R1",40,30,60,60,2,
            10);
    builder.addMove("R1",10,20,200,100,12,
            15);
    builder.addColorChange("R1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,13,14);
    builder.addScaleToChange("R1",10,17,20,40,17,
            30);
    builder.addScaleToChange("O1",10,17,20,40,17,
            30);
    builder.addMove("O1",60,60,100,100,20,
            23);
    builder.addColorChange("O1",0.3f,0.7f,0.2f,0.1f,0.3f,
            0.7f,24,27);
    builder.addMove("O1",10,20,200,100,30,
            35);

    Animator model = (Animator)builder.build();

    List<AbstractShape> shapes = model.getShapes();
    List<AbstractAnimation> animations = model.getAnimations();

    textual = new Textual();
    textual.initialize(shapes, animations);
    textual.render(0,0,1,new PrintStream(out));

    assertEquals(output, out.toString());
  }
}