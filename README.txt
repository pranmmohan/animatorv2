Prannoy Mohan & Sean Choudhary


Changes Made:

  Views:
  We seperated the functions within SVG and View so that our new views that extend them,
  SVGloop which is for creating a loop svg, and Hybrid which extends visual can use the functions
  available in the extended from class.
  Model:
  We  removed our priority queue for the
  shapes according to their star times because it was causing our shapes to be painted in the wrong
  order for shapes that started at the same time. We also changed changed the position and
  dimension calculation for ovals.

Design:
  model:
  Two shapes:
  Rectangle and Oval
  Each shape has its own list of animations
  Animaions:
  ReSize = changes the dimensions
  ChangeColor = changes the color
  Move = changes the position of the shape

  View:
  There is a hashmap that maps the string to a boolean, it is used during rendering
  to see if the shape should be rendered.
  Views have two functions:
    Initalize = to pass the set of shapes and animations
    Render = to run the view according to the tick rate
  HybridView:
    extends the visual view and implements the interactive view
    , and has functions from the controller that allow it to respond to the controllers actions.

  Controller:
    Has the functionality that enables the view to be run, stopped midway, started again,
    restarted, export an svg on click, hide shapes and exit.
    We decided to use a GUI with event listeners for clicks. There is a start/stop button,
    restart button, loop checkbox, shape selection checkbox, and exit button.
    the start, stop interact with the timer
    restart reverts the shapes attribute such as color, and position back to their original forms
    and restarts the timer
    loop adds a condition that gets executed when the tick surpases the total time.
    the svg is made to the file name entered through the gui only when the make svg button is
    pressed.
    We do not have keyboard listeners as we use buttons.


